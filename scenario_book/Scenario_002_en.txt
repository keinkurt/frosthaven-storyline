The scenario is complete when all |
ememiies in it are dead and ice sheet @
has Deen destroyed. At the end of that
round, read [§ 77.3.

introduction

“They'll be tired from the raid, so this
is your moment.” Satha had explained,
pointing you toward the foothills of the
Copperneck Mountains where her scout
tracked the Algox attackers. “I doubt
you'll be able to fight them all on your
own, but maybe you can find another
way to subdue them if you get close. On
the other hand, they’! be worn out after
this little battle, so feel free to test your
luck. Just remember, even a tired Algox is
strong enough to rip someone in two.”

You head to the area indicated by the
scout, following the Algox’s trail toward
a wicked-looking mountain. As you draw
mearer you see that the trail splits: the
main force returned to the near side of the
mountain, but a smaller group peeled off
to hike around to the eastern face.

The scout told you he suspected there was.
a back door to the Algox settlement, and ~

you follow the smaller group’s trail hoping...

he was right.

The path narrows as-you approach the —
‘steep eastern face, winding up to an
overlook high above. Peering upward, you
make out the faintest glimmer of light, but
then a shape blots it out. A body, large and
white, tumbles over the rocky ledge like

a botilder, slamming against the jutting
rocks: A dead Algox. brawler, just like —
the ones who attacked Frosthaven, lies ~
at your feet. Judging by its injuries, the —
rocks weren’t its only problem—there’s a
fight up above. You scramble toward the
top, hoping to make it before the battle is
through. When you’re halfway up, another

furry shape appears over the ledge.

“More intruders.” she scoffs. “You: will

never open the Skyhall. This place belongs

to the Ice!” Your shock at the fact that an

Algox just spoke your tongue is quelled as
the speaker lifts a huge boulder and hurls
it Gown at you. Time to move.

_ Algox Guard

_Algox Priest

|  Algox Scout

Snow Door

Snow Rock

Treasure

Dodging boulders on the side of a “Quickly then, warm-bloods—help me Special Rules

MOUnh2in wasn't quite how you imagined fight these dirt-lovers. Aid me, and I swear The Algox Priest is an ally to you and
the morning, but you make it to aid you.” Her hand, though pinned, an enemy to all other monsters. If all
a= the overlook without getting crushed. glows with a crackling blue energy. revealed enemies are dead, the Algox
Hiowewer, just before you pull yourself Priest focuses on moving toward and
ower the final ledge, a voice carries over opening door @. If the Algox Priest
the wind, stopping you. : _ | dies, the scenario is lost.

“Poul ice-pissers!” It shouts in a.deep,
Commanding cry. “You're nothing but
petty Dlasphemers twiddling in your
—

Section Links

When door @ is opened, read 6.1.

Z SN
t —
J le ~~
Z Mi
4 wt 4

=== ou make it over the ledge and see the

== Owner of the voice pinned between two
==  oeiders-she’s.clearly an Algox, but she
S$ Gressed much differently fromthe
Srates who were just hurling boulders at
* oe. Larger than her male opponents, she
== Wears ormate battle garb that marks her as

= prominent member of a different faction.

== Sh turns to you with snarling hatred,

= ‘ot softens when she realizes you’re not
= ends of her enemies.

Mi 27.2 - Derelict Elevator (16)

la 27.3 - Sacred Soil (69)

WSs! were once pebbles now are stones Conclusion

@me cracked brick. Fist-sized hunks of

j= sock have begun to fall from above and

[ en crash loudly onto the metal floor: ‘it.
== Seems the shaftis collapsing. You keep

= clear of the shower, but now more debris is

ome to fall elsewhere.

ae ks a a

man Rules

Carrying a case laden with soil samples
from all about the forest, you trek back

to Liseritus’ humble abode and sit to
enjoy one more cup of tea as it sets about
examining the samples. Using an array of
magnifying glasses, Liseritus peers down
at the soil, picking at it with tweezers,
muttering, and scribbling notes.

eerie aac oumeomercen ie srpenpmmmmeen onemeee en ee

Not even breaking from its work, Liseritus
breaks the silence. “This is going to take
longer than I thought. I will send for you
when I have an answer.”

Add (15.4 to pegs ree
weeks. ;

ee

Everyihumg is set. Craim’s gyroscopic
WIEDEN S repped. A vial of dark
energy Mumm om its desk. And the device,
he ome that willl create these symbols of
power, 2 moumied imside the gyroscope,
wait ap be charged From here it looks
ite 2 ship's wheel bolted atop a tripod, but
l@stead of wood, the wheel is made of all
Wamety of crystal and glass and metal.

ow anc Crain are seated by the fire, texts
@mc Gocuments stacked all around as you
© © Seknot the final riddle from Project
Semrce: the calibration instructions.

The workshop has’been quiet for an hour,
©scept for the oCcasional crackles from the
Sreplace, when’suddenly Crain lunges up
‘rom his chair and lets out a groan.

"we hit the wall,” he says. “My brain is‘a
Seg of oats. It’s bad now. Bad brain, that’s

wi 77.3 
Aigox Scouting (2)

| Conclusion

The ice barrier shatters with a ‘great earSpitting crack and you look down through
te Bole to a chamber far below. A stone

= = or covered in strange runic symbols and

pe

= pital of pedestals radiating out from its.
Center—Skyhall, it would-seem. However,
= much too far to simply jump.

| “Ha! Finally it’s open.” You turn to see __ ry

the Algox you.rescued staring forward.

sme mods with a confident, satisfied ian x

“These foul Icespeakers cannot poets COs
» Beep this mountain now...’

3 ( She bends down and grabs a itil

_ Of sow, testing it between her fingers.
Satisited, she brings it to her face and

whispers. The white powder flies from heh
—=_— and swirls around her. Whirling like ~

@ M@istwre storm, the snow lifts her off
= Spoenc and carries her gently down

p Sroneh the hole.

“AR” Ge Algox calls to you from below, | “pe wou mot coming? enty more
4 @ kill”

owe he smow around you swirls into
@ est eiity of white and lifts you like
@hket carowe vou down through the

what I’ve-got. Maybe it'was those caves,
you know? Not easy on the mind, all that
hallucinating and whatnot.”

He lurches over to the fire and prepares
another pot of tea. You turn your attention
back to the book. The riddle, you assume,
was left as a sort of protection: a final bit
of cypher to prevent the wrong person
from replicating Logren’s work. The
cypher itself isn’t terribly difficult, but
you’ve not been able to figure out where
to draw the reference material. You pull
over Crain’s notes and see if, somehow, it
makes sense this time.

Turn to the next page in the puzzle
book.

opening just like the Algox. As you float ~
down, you get a grand view of the majestic
open cavern that ‘must be the Skyhall. It

is a circular layout of stone seats with a
raised platform in the center that holds an
ornate altar,

The priest is doing something at the
altar—perhaps praying—when you land,

but she quickly turns and introduces
herself as Lanprul, explaining the
e sna

ge According t to hee this mountain—
_ Snowscorn—is a site of great religious
significance toall Algox. However, the
Clans in the area are divided into two
rival factions who disagree violently

over how to communicate with their god.

: The Snowspeakers—led by the priest in
_ front of you—believe their god speaks to

them through the snow falling from the
heavens, while the Icespeakers—or “dirt
’ lovers”—claim to commune with divinity —
_ through ice that emerges from the ground

below.

Currently, the Icespeakers inhabit the
mountain, but when their war party went
out to attack-Frosthaven and returned
battle-weary, the Snowspeakers used the

“Willful, willful Lurkers! They have an
abundance of stamina and intelligence, but
accompanied by such willfulness! When

_ I tried to drain their energy, their psychic

power nearly tore me apart from the

__ inside. Horrible creatures!”

opening to launch an attack of their own.

“But there is no more time for speech,” she
says decisively. “The battle must be raging
in the mountain’s heart, so if you want my
help, we must head there with all haste.”
With that, the Snowspeaker chieftain turns
and-moves to a tunnel leading deeper into
the mountain. —

You’re not sure which side you should
take here, but you know this information
is valuable. If you can help turn the tide

in the Snowspeakers’ favor, you might be
able to broker a truce with the victors. You
follow the priest deeper into the mountain,
wondering whether this i is what Satha had
in mind.

Gain 1 ¥ each.
Place map overlay sticker W on the
map in location W (M7).

New Scenario:
Heart of Ice (4) tx)

Locked Out Scenario:
Algox Offensive (3)
