# Szenario-Ziele
Das Szenario ist abgeschlossen, wenn alle Feinde darin sind tot. Am Ende der dieser Runde lies 9.1

# Einführung

Die Geräusche begrüßen dich zuerst: dumpfe
metallisches Klopfen, eine heulende Stimme, es ist
Es ist schwierig, etwas zu verstehen, wenn der
Wind, der an deinen Ohren vorbeirauscht, aber du würdest
aber du würdest diese Melodie überall erkennen: da vorne ist ein Kampf
da vorne,

Aber ein Kampf bedeutet Menschen, und wenn es Menschen gibt.
Menschen, dann muss die Stadt nah sein, Du
hörst du mehr: einen Schrei; das Krachen von Stahl
gegen Stein; ein tiefes, erdiges Knurren.

Du läufst jetzt. Deine Beine sind müde
von der Wanderung, deine Schultern schmerzen von
der Ausrüstung, aber die Geräusche sind jetzt nah
und vor dir siehst du hauchdünnes orangefarbenes Licht

Mit einem letzten Schubs umrundest du die letzte Kurve
um die letzte Kurve des Weges und du siehst

Es - Frosthaven - ging in Flammen auf,

# Eine Stadt in Flammen

Große Feuerblüten sprengen aus den Fenstern
und kriechen über Dächer, Menschen strömen aus
dem zerstörten Stadttor, die Gesichter in Panik verzogen
Einen Moment später sieht man das Objekt
eine riesige Kreatur mit zwei Beinen
und zwei massigen Armen, die ganz mit
dickem weißen Fell, drei knorrige
Hörner krönen seinen Kopf,

"Algox-Raider! Wir sind dem Untergang geweiht!" schreit

ein älterer Pförtner neben Ihnen. "Sie sind
stärker als alles andere auf der Welt.
den ganzen Weg umsonst gekommen!" Wie zum Beweis
der Algox, den du beobachtet hast.
fängt der Algox, den ihr beobachtet habt, einen fliehenden Emporkömmling mit
einer Hand und schleudert ihn nach hinten, als ob er
als ob er nichts wöge,

Zehn Tage bist du gewandert. Zehn Tage
und das ist, was du findest. Du atmest aus
eine dicke Wolke nebligen Alkohols aus und hebst deine
und hebst deine Waffe hoch. Zeit, an die Arbeit zu gehen.

# Besondere Regeln

Alle Stadtwachen sind Verbündete für dich und
Feinde für alle anderen Monstertypen.
Stadtwachen führen keine Züge aus, sondern
stattdessen erhalten sie ein zusätzliches angeborenes @ i
(für eine Gesamtzahl von @ 2 auf Stufe 1) und @ 2,
und haben eine Initiative von 50 für den Zweck der Fokussierung.

# Abschnitt Links
Wenn die Tür @ geöffnet wird, lesen Sie die §§ 5-3.

# Eine Stadt in Flammen (1)

Sie laufen durch das Tor und das Ausmaß der
Zerstörung wird schnell deutlich.
Die Stadt ist geplündert worden. Der Rauch verbrennt
deine Augen. Die Gebäude, die noch stehen
sind hell erleuchtet, und der Rest schwelt vor sich hin,

ihre Rahmen wie schwarze, abgefackelte Skelette.
Und überall in den Trümmern heulen die
Algox und feierten ihren Sieg in einer
unerkennbaren Sprache.

# Eine Stadt in Flammen (2)

Du senkst deine Waffe und atmest, während die
verbliebenen Algox sich zurückziehen und aus
Frosthaven wie riesige weiße Mäuse. Sie werden
zurückkommen, da bist du dir sicher, aber im Moment
kannst du dich ausruhen. Du wischst dir den Ruß aus dem Gesicht
und ziehst Bilanz.

Frosthaven ist fast genau das, was Sie sich
vorstellten: ein Knoten aus grauem Stein und
Holz, umgeben von geschliffenen Palisaden
Mauern - ein Ort, an dem das bloße Überleben

eine alltägliche Angelegenheit, in der sich nur die
verzweifelte Menschen sich zu Hause fühlen können. Zum Glück
sind die Bewohner der Stadt jedoch zäh. Sie sind
Sie sind bereits auf den Beinen, löschen Brände und
wühlen sich durch die Trümmer. Tatsächlich marschiert einer der
dieser Stadtbewohner direkt auf dich zu
auf dich zu; eine scharf geschnittene menschliche Frau
muskulösem Körperbau, kurzgeschorenem Haar
und dunkler Haut, kommt auf dich zu und reicht dir
eine stumpfe Hand zur Begrüßung. "Das Meer sei
gepriesen", sagt sie und zerdrückt deine Finger
mit Begeisterung. "Ohne dich hätten wir nicht
ohne dich nicht länger durchgehalten."

Sie nicken und fragen, was passiert ist.

"Ach, das ist das Leben im Norden", kichert sie,
"Ich bin Satha, Bürgermeisterin dieses Forts, das jetzt

mein Vorgänger und sein Leutnant sind
gefallen. Was die Algox betrifft, so haben sie uns
den ganzen Winter über. Sie brauchten Monate, um durchzubrechen.
durchzubrechen, aber sie haben es endlich geschafft. Sie töteten ein
gutes Dutzend meiner Leute getötet... und es wäre
und es wäre noch schlimmer gekommen, wenn du nicht aufgetaucht wärst."

Sie erklären, wer Sie sind und dass Sie
von White Oak geschickt worden. Bei der Erwähnung
der Hauptstadt, ihr Gesicht verzieht sich vor Unmut.

"Es ist an der Zeit, dass sie jemanden schicken, der hilft
um den Schlamassel zu beseitigen, den sie angerichtet haben", sagt sie und
versucht sie, ihre Stimmung aufzulockern. "Ich will nicht.
undankbar sein. Ich bin überglücklich, dass ich noch
unter den Lebenden zu sein, aber viel mehr Menschen

wären noch am Leben, wenn Ihre Arbeitgeber, die
Kaufmannsgilde, ihre Nasen nicht in unsere
in unser Geschäft stecken würden.

"Ich habe mein ganzes Leben hier verbracht, in einem
Fischerdorf verbracht, lebte und ließ
mit den Algox zu leben. Bis zum letzten Sommer,
als die Handelsgilde ein paar
einige Garnisonen und Bürokraten-Typen

hier oben, um zu sehen, wie Frosthaven "dem
für das Imperium von größerem Nutzen sein". Eine ihrer
Expeditionen machten sich an einer
in einer Algox-Grabstätte, und als Nächstes
greifen uns rechtschaffen wütende Algox
uns angreifen. Die Tintenflecken sind sofort
abgehauen und haben nur eine Handvoll Soldaten
eine Handvoll Soldaten zurück, um uns zu beschützen."

Satha drückt ihren Handschuh immer fester zu
fester, während sie die Geschichte erzählt, aber jetzt
lockert sie ihren Griff. "Ich musste viel lernen
im letzten Jahr viel lernen. Die harte Wahrheit ist
dass die Algox sich weigern, mit sich reden zu lassen,
und der einzige Weg, am Leben zu bleiben und diesen Ort.
und diesen Ort, den ich mein Zuhause nenne, zu behalten, ist, den Kampf zu gewinnen,
unabhängig davon, wer ihn begonnen hat."

Ihr Gesicht wird weicher und sie umklammert Ihre
Schulter, fest. "Es gibt nicht viele von euch,
aber ich nehme, was ich kriegen kann, besonders jetzt,
wo unsere Garnison ausgedünnt wurde. Und zufälligerweise habe ich eine Idee."

Sie dreht sich um und gestikuliert zu den schwelenden Mauern.

"Wir haben in diesem Kampf eine Tracht Prügel bezogen, also wenn wir noch länger überleben wollen,
müssen wir mehr tun, als nur herumzusitzen und
auf den nächsten Angriff warten." Sie mustert dich
sorgfältig ab. "Ich habe bereits einen Späher losgeschickt.
um den Algox zurück in die Berge zu verfolgen.
Lasst eure Ausrüstung in jedem Langhaus, das noch steht
und ruht euch aus. Sobald ihr fertig seid,
will ich, dass ihr dorthin geht und einen Weg findet.
einen Weg zu finden, diese Angriffe zu verlangsamen."

Die Kojen in den übrigen Langhäusern
sind alle von den Verletzten belegt, aber ihr
findet ein trockenes Stück Boden, auf dem ihr eure Köpfe
auf euren Rucksäcken. Ihr bezweifelt, dass ihr schlafen könnt
nach dem Schrecken, der euch in diesem kleinen
Außenposten, aber bevor du es merkst, stupst dich ein
junger Valrath-Mann euch wachrüttelt:
Der Späher von Satha ist mit seinem Bericht zurückgekehrt.

Er verfolgte die Angreifer bis zu den Ausläufern

der Copperneck-Berge, wo sie eine Höhle
in eine Höhle am Fuße des Snowscorn
Berg ©. Interessant ist, dass sich eine kleinere
Gruppe von der Hauptgruppe ab, bevor sie die
bevor sie die Höhle erreichten, und wanderte zur
tödlichen Ostwand des Berges@). Der
Scout hat nicht gesehen, wohin sie gingen, aber er
vermutet einen geheimen Eingang zur Algox
Festung.

"Eine letzte Sache", sagt der Valrath, während du
die letzten Reste des Schlafs aus den Augen wischt.
"Eine weitere Gruppe bewegt sich auf
Snowscorn. Zweifellos waren es Algox,
aber sie sind anders gekleidet als die.
diejenigen, die uns angegriffen haben. Sie trugen lange
Stöcke und hatten einige wilde Tiere bei sich
mit sich. Ich weiß nicht, ob sie den Berg
angreifen oder ihn verstärken, aber
auf jeden Fall solltest du schnell dorthin kommen."

Erhalte 2+X Moral, wobei X die
Anzahl der Stadtwachen, die noch auf der
Karte,

# Neue Szenarien:
Algox Scouting (2),
Algox-Offensive (3)

Sie führen nun eine Außenposten Phase (lesen Sie die Kampagnenregeln ab S. 50 des Regelbuchs),
