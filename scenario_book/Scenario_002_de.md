Das Szenario ist abgeschlossen, wenn alle |
emiies in ihm tot sind und das Eisschild @
Deen zerstört hat. Am Ende dieser
Runde, lies [§ 77.3.

# Einführung

"Sie werden von der Razzia müde sein, also ist dies
ist das dein Moment." hatte Satha erklärt,
und dir den Weg zu den Ausläufern der
Copperneck Mountains, wo ihre Späher
die Algox-Angreifer aufspürte. "Ich bezweifle
Ich bezweifle, dass du sie ganz allein bekämpfen kannst.
aber vielleicht kannst du einen anderen Weg finden
einen anderen Weg finden, sie zu überwältigen, wenn du in ihre Nähe kommst. Auf
Andererseits sind sie nach diesem Kampf erschöpft.
diesem kleinen Kampf erschöpft sein, also kannst du dein
Glück. Denk daran, dass selbst ein müder Algox
stark genug, um jemanden in Stücke zu reißen."

Du gehst zu dem vom Späher angegebenen Gebiet
und folgst der Spur des Algox zu einem
einen bösartig aussehenden Berg. Als du dich
näher kommst, siehst du, dass sich die Spur teilt: Die
Haupttruppe kehrte auf die nahe Seite des
des Berges zurück, aber eine kleinere Gruppe hat sich
um zur Ostseite zu wandern.

Der Späher hat dir gesagt, dass er einen Verdacht hat.
eine Hintertür zur Algox-Siedlung, und ~

Du folgst der Spur der kleineren Gruppe und hoffst...

er hatte Recht.

Der Pfad wird schmaler, wenn du dich der -
steilen Ostwand und schlängelt sich bis zu einem
Aussichtspunkt hoch oben. Wenn du nach oben schaust, kannst du
kannst du einen schwachen Lichtschimmer ausmachen, aber
doch dann wird er von einer Gestalt ausgelöscht. Ein Körper, groß und
weißer Körper, der über den Felsvorsprung stürzt wie

ein Botilder, der gegen die vorspringenden Felsen knallt
Felsen: Ein toter Algox. Streiter, genau wie -
wie die, die Frosthaven angegriffen haben, liegt ~
zu deinen Füßen. Seinen Verletzungen nach zu urteilen, waren die -
Felsen nicht sein einziges Problem - da oben gibt es einen
Kampf da oben. Du kletterst nach oben
und hoffst, dass du es schaffst, bevor der Kampf vorbei ist.
vorbei ist. Als du auf halbem Weg nach oben bist, kommt ein

erscheint eine pelzige Gestalt über dem Felsvorsprung.

"Noch mehr Eindringlinge", spottet sie. "Du: wirst niemals die Skyhall öffenen. Dieser Ort gehört zum Eis!" Dein Schock über die Tatsache, dass ein
Algox hat gerade gesprochen und deine Zunge ist verstummt, als
der Sprecher einen riesigen Felsbrocken anhebt und
und ihn auf dich schleudert. Zeit, sich zu bewegen.

# 27.1

Ausweichen vor Felsbrocken an der Seite eines "Schnell, ihr Warmblüter - helft mir 

# Sonderregeln

MOUnh2in war nicht ganz so, wie du dir den Kampf gegen diese Dreckskerle vorgestellt hast. Hilf mir, und ich schwöre, der Algox-Priester ist ein Verbündeter von dir und
den Morgen, aber du bringst ihn dazu, dir zu helfen." Ihre Hand, obwohl sie gefesselt ist, ein Feind für alle anderen Monster. Wenn alle
a= die Übersicht, ohne zerquetscht zu werden. leuchtet mit einer knisternden blauen Energie. aufgedeckten Feinde tot sind, ist der Algox
Hiowewer, kurz bevor du dich Priester konzentriert sich auf die Bewegung zum und
über den letzten Felsvorsprung, ertönt eine Stimme, die die Tür @ öffnet. Wenn der Algox-Priester
_ | stirbt, ist das Szenario verloren.

"Poul Eis-Pisser!" Es schreit in einem tiefen,
befehlenden Schrei. "Ihr seid nichts als
Dlasphemers, die in ihrem
-

Abschnitt Links

Wenn die Tür @ geöffnet wird, lies 6.1.

Z SN
t -
J le ~~
Z Mi
4 wt 4

=== Du schaffst es über den Vorsprung und siehst die

== Die Besitzerin der Stimme, eingeklemmt zwischen zwei
== oeiders - sie ist eindeutig eine Algox, aber sie
S$ war ganz anders als die
Srates, die gerade Felsbrocken auf
* oe. Sie ist größer als ihre männlichen Gegner und
== Sie ist größer als ihre männlichen Gegner und trägt ein Kampfgewand, das sie als

= prominentes Mitglied einer anderen Fraktion.

== Sh wendet sich dir mit knurrendem Hass zu,

= Sie wird weicher, wenn sie merkt, dass du nicht
= Enden ihrer Feinde bist.

Mi 27.2 - Verfallener Aufzug (16)

la 27.3 - Heiliger Boden (69)

WSs! waren einst Kieselsteine und sind jetzt Steine Fazit

@me gesprungener Ziegelstein. Faustgroße Klumpen von

j= Socke haben begonnen, von oben zu fallen und

[ en krachen laut auf den Metallboden: 'it.
== Es scheint, dass der Schacht zusammenbricht. Du hältst

= von der Dusche entfernt, aber jetzt ist mehr Schutt

ome anderswo zu fallen.

ae ks a a

Mann Regeln

Du trägst einen Koffer voller Bodenproben
aus dem ganzen Wald, wanderst du zurück

zu Liseritus' bescheidener Behausung und sitzen
noch eine Tasse Tee zu genießen, während er sich daran macht
die Proben zu untersuchen. Mit einer Reihe von
Vergrößerungsgläsern schaut Liseritus auf die
auf den Boden und stochert mit einer Pinzette darin herum,
murmelt und kritzelt Notizen.

eerie aac oumeomercen ie srpenpmmmmeen onemeee en ee

Ohne seine Arbeit auch nur zu unterbrechen, bricht Liseritus
bricht das Schweigen. "Das wird länger dauern
länger dauern, als ich dachte. Ich werde nach dir schicken
wenn ich eine Antwort habe."

Addiere (15,4 zu den Pflöcken ree
Wochen. ;

ee

Alles ist bereit. Craims Kreisel ist
WIEDEN S repped. Ein Fläschchen mit dunkler
Energie Mumm om seinem Schreibtisch. Und das Gerät,
das Gerät, das diese Symbole der Macht erschaffen wird
Macht, 2 Mumien im Inneren des Gyroskops,
wartet darauf, aufgeladen zu werden Von hier aus sieht es aus
wie 2 Schiffsräder, die auf einem Dreibein befestigt sind, aber
l@statt aus Holz ist das Rad aus allen
aus Kristall, Glas und Metall.

ow anc Crain sitzen am Feuer, Texte
@mc Gocuments rundherum gestapelt, während du
© © Seknot das letzte Rätsel von Project
Semrce: die Kalibrierungsanweisungen.

In der Werkstatt ist es schon seit einer Stunde still,
bis auf das gelegentliche Knistern des
Kamin, als Crain plötzlich von seinem Stuhl aufspringt
von seinem Stuhl auf und stöhnt auf.

"Wir sind gegen die Wand gefahren", sagt er. "Mein Gehirn ist ein
Haferbrei. Es ist jetzt schlecht. Schlechtes Gehirn, das ist

wi 77.3
Aigox Scouting (2)

| Fazit

Die Eisbarriere zerbricht mit einem ohrenbetäubenden Knall und du schaust durch den
te Bole in eine Kammer weit unten. Ein Stein

= = oder mit seltsamen Runensymbolen bedeckt und

pe

= pital von Sockeln, die strahlenförmig von seiner.
Center-Skyhall, so scheint es. Aber,
= viel zu weit, um einfach zu springen.

| "Ha! Endlich ist es offen." Du drehst dich um und siehst __ ry

die Algox, die du gerettet hast, starrt nach vorne.

sme mods mit einem selbstbewussten, zufriedenen ian x

"Diese üblen Eissprecher können nicht dichten COs
" Beep this mountain now...'

3 ( Sie bückt sich und schnappt sich ein Itil

der Sau und testet sie zwischen ihren Fingern.
Zufrieden hält sie es an ihr Gesicht und

flüstert. Das weiße Pulver fliegt von ihr weg
-=_- und wirbelt um sie herum. Es wirbelt wie ~

@ M@istwre Sturm, der Schnee hebt sie ab
= Spoenc und trägt sie sanft hinunter

p Sroneh das Loch.

"AR" Ge Algox ruft dir von unten zu, | "pe wou mot coming? enty more
4 @ töten"

owe he smow around you swirls into
@ est eiity of white und hebt dich wie
@hket carowe vou hinunter durch die

was ich bekommen habe. Vielleicht waren es diese Höhlen,
weißt du? Es ist nicht leicht für den Geist, all das
Halluzinationen und so weiter."

Er taumelt zum Feuer und bereitet eine
eine weitere Kanne Tee. Du wendest deine Aufmerksamkeit
wieder auf das Buch. Das Rätsel, nimmst du an,
als eine Art Schutz hinterlassen wurde: ein letztes Stück
um zu verhindern, dass die falsche Person
Logrens Arbeit nachzumachen. Die
Chiffre selbst ist nicht besonders schwierig, aber
du konntest nicht herausfinden, wo
wo du das Referenzmaterial hernehmen sollst. Du nimmst
die Notizen von Crain durch und überlegst, ob es
dieses Mal einen Sinn ergibt.

Schlag die nächste Seite im Rätselbuch auf
Buch.

öffnet sich genau wie das Algox. Wenn du hinabschwebst ~
hinunterschwebst, hast du einen großartigen Blick auf die majestätische
offene Höhle, die die Skyhall sein muss. Sie

ist eine kreisförmige Anordnung von Steinsitzen mit einer
erhöhten Plattform in der Mitte, auf der ein
verzierten Altar,

Der Priester tut gerade etwas am Altar
Wenn du landest, betet er vielleicht gerade am Altar,

aber sie dreht sich schnell um und stellt sich
sich als Lanprul vor und erklärt die
e sna

ge Nach hee dieser Berg-
_ Snowscorn- ein Ort von großer religiöser
Bedeutung für alle Algox. Allerdings sind die
Clans in diesem Gebiet sind in zwei
rivalisierende Fraktionen, die sich heftig streiten

darüber, wie sie mit ihrem Gott kommunizieren können.

: Die Schneesprecher - angeführt von dem Priester, der
vor dir - glauben, dass ihr Gott zu ihnen spricht.

sie durch den Schnee, der vom Himmel
Himmel, während die Eissprecher - oder "Dreck
Liebhaber" - behaupten, dass sie mit der Gottheit kommunizieren
_ durch Eis, das aus dem Boden aufsteigt

unten.

Derzeit bewohnen die Eissprecher den
Berg, aber als ihr Kriegstrupp ausrückte
Frosthaven angreifen wollte und
zurückkehrte, nutzten die Schneesprecher den

"Eigensinnige, eigensinnige Schleicher! Sie haben eine
Ausdauer und Intelligenz im Überfluss, aber
begleitet von solcher Willkür! Wenn

Ich habe versucht, ihre Energie, ihre psychische

Macht riss mich fast von der

__ innen. Schreckliche Kreaturen!"

um einen eigenen Angriff zu starten.

"Aber wir haben keine Zeit mehr zum Reden", sagt sie
sagt sie entschlossen. "Die Schlacht muss im Herzen des Berges toben.
im Herzen des Berges toben, wenn du also meine
Wenn du also meine Hilfe willst, müssen wir uns schnellstens dorthin begeben."
Mit diesen Worten dreht sich die Schneesprecher-Häuptlingstochter
und geht zu einem Tunnel, der tiefer in den
den Berg führt. -

Du bist dir nicht sicher, auf welcher Seite du
nehmen soll, aber du weißt, dass diese Informationen
wertvoll sind. Wenn du helfen kannst, das Blatt zu wenden

zu Gunsten der Snowspeakers ausfällt, könntest du
einen Waffenstillstand mit den Siegern aushandeln können. Du
folgst dem Priester tiefer in den Berg hinein,
und fragst dich, ob es das ist, was Satha
im Sinn hatte.

Gewinnt jeweils 1 ¥.
Platziere den Kartenaufkleber W auf der
Karte an der Stelle W (M7).

Neues Szenario:
Herz aus Eis (4) tx)

Aussperrungs-Szenario:
Algox-Offensive (3)
