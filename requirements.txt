deepl==1.14.0
packaging==23.0
pdf2image==1.16.3
Pillow==9.4.0
pytesseract==0.3.10