#!/usr/bin/env python3
import pytesseract
import deepl
from PIL import Image
from pathlib import Path
from pdf2image import convert_from_path
from tempfile import TemporaryDirectory


def main():
    """ Main execution point of the program """
    pdf_file = Path(r"scan/Scenario_003.pdf")
    en_file = Path("scenario_book/Scenario_003_en.txt")
    de_file = Path("scenario_book/Scenario_003_de.txt")
    image_file_list = []

    with TemporaryDirectory() as tempdir:
        # Create a temporary directory to hold our temporary images.

        """
        Part #1 : Converting PDF to images
        """

        pdf_pages = convert_from_path(pdf_file, 500)
        # Read in the PDF file at 500 DPI

        # Iterate through all the pages stored above
        for page_enumeration, page in enumerate(pdf_pages, start=1):
            # enumerate() "counts" the pages for us.

            # Create a file name to store the image
            filename = f"{tempdir}\page_{page_enumeration:03}.jpg"

            # Declaring filename for each page of PDF as JPG
            # For each page, filename will be:
            # PDF page 1 -> page_001.jpg
            # PDF page 2 -> page_002.jpg
            # PDF page 3 -> page_003.jpg
            # ....
            # PDF page n -> page_00n.jpg

            # Save the image of the page in system
            page.save(filename, "JPEG")
            image_file_list.append(filename)

        """
        Part #2 - Recognizing text from the images using OCR
        """

        with open(en_file, "a") as output_file:
            # Open the file in append mode so that
            # All contents of all images are added to the same file

            # Iterate from 1 to total number of pages
            for image_file in image_file_list:
                # Set filename to recognize text from
                # Again, these files will be:
                # page_1.jpg
                # page_2.jpg
                # ....
                # page_n.jpg

                # Recognize the text as string in image using pytesserct
                text = str(((pytesseract.image_to_string(Image.open(image_file)))))

                # The recognized text is stored in variable text
                # Any string processing may be applied on text
                # Here, basic formatting has been done:
                # In many PDFs, at line ending, if a word can't
                # be written fully, a 'hyphen' is added.
                # The rest of the word is written in the next line
                # Eg: This is a sample text this word here GeeksF-
                # orGeeks is half on first line, remaining on next.
                # To remove this, we replace every '-\n' to ''.
                text = text.replace("-\n", "")

                # Finally, write the processed text to the file.
                output_file.write(text)

        """
        Part #3 - Translate with DeepL
        """

        translator = deepl.Translator(r"5c6bfa58-3761-dfb2-0aad-e679377be3a3:fx")
        translator.translate_document_from_filepath(en_file, de_file, formality='less', target_lang='DE')


if __name__ == "__main__":
    # We only want to run this if it's directly executed!
    main()
